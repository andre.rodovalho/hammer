# Hammer

A simple bash script to help you perform Stress / Load / Rate-limit testing.

This script helps creating lots of HTTP requests to a given URL. These requests run in the background, in parallel. Be aware this may create a high load on the system running it.

## Dependencies

This depends on `curl` and you may need to install it on your system

## Usage

Place the script into a directory on your PATH (e.g.: `~/.local/bin/` or `/usr/bin/`) and make it executable. The script expects a parameter: the URL

```
$ hammer http://localhost
```

To stop execution, use `Ctrl + c`
